const ethers = require('ethers')
const expect = require('chai').expect
let IdManager = require('../lib/idManager')
let Parity = require('@caelum-tech/parity')
const IdJSON = require('@caelum-tech/identity-manager-contracts/build/contracts/Identity.json')

let idManager, wallet, parity

describe('Identity Library', () => {
  describe('Using the Library Factory', () => {
    before(async () => {
      parity = new Parity('http://localhost:8545')
      expect(await parity.isBlockchainRunning()).to.be.equal(true)
      wallet = await parity.loadWallet('0x8ad0ba67f8088d1f990f878815173f1dafda0a55', '59Ta_ad6_]k<PWPp')

      // Depoly IdManager and create one identity
      idManager = new IdManager(parity, wallet)
      await idManager.deployFactory()
      await idManager.addIdentity('test', wallet.address)
    })

    it('Should setData and getData for one identity', async () => {
      const id1 = await idManager.getIdentity('test')
      id1.setWallet(wallet)
      await id1.setData('test', 'Hello World')
      expect(await id1.getStringData('test')).to.be.equal('Hello World')
    })

    it('Should setAddress and getAddress for one identity', async () => {
      const id1 = await idManager.getIdentity('test')
      id1.setWallet(wallet)
      await id1.setAddress('testaddr', wallet.address)
      expect(await id1.getAddress('testaddr')).to.be.equal(wallet.address)
    })

    it('Should test Root Identity', async () => {
      let wallet = await parity.loadWallet('0x8ad0ba67f8088d1f990f878815173f1dafda0a55', '59Ta_ad6_]k<PWPp')
      let rootId = await parity.getContract(Parity.getSystemAddresses().identityContract, IdJSON.abi, wallet)
      const keyManager = ethers.utils.formatBytes32String('IdManager')

      await rootId.setAddress(keyManager, wallet.address)
      expect(await rootId.getAddress(keyManager)).to.be.equal(wallet.address)
  
    })

  })
})
