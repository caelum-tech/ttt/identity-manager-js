const expect = require('chai').expect
let IdManager = require('../lib/idManager')
let Parity = require('@caelum-tech/parity')

let idManager, wallet, parity

describe('idManager Library', () => {
  describe('Using the Library Factory', () => {
    before(async () => {
      parity = new Parity('http://localhost:8545')
      expect(await parity.isBlockchainRunning()).to.be.equal(true)
      wallet = await parity.loadWallet('0x8ad0ba67f8088d1f990f878815173f1dafda0a55', '59Ta_ad6_]k<PWPp')
    })

    it('Should Deploy the Factory Contract', async () => {
      idManager = new IdManager(parity, wallet)
      await idManager.deployFactory()
      expect(idManager.contract).to.not.be.equal(null)
      expect(await idManager.population()).to.be.equal(0)
    })

    it('Should add a new Identity', async () => {
      const id = await idManager.addIdentity('test', '0x8ad0ba67f8088d1f990f878815173f1dafda0a55')
      const addr = await idManager.resolve('test')
      expect(id.contract.address).to.be.equal(addr)
    })

    it('Should resolve the identity', async () => {
      const id = await idManager.getIdentity('test')
      let identity = await idManager.resolve('test')
      expect(identity).to.be.equal(id.address)
    })

    it('Should get the population', async () => {
      expect(await idManager.population()).to.be.equal(1)
    })
  })
})
