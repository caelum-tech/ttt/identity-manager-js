const ethers = require('ethers')
const IdentityContract = require('@caelum-tech/identity-manager-contracts/build/contracts/Identity.json')

/**
 * Class Identity
 * It holds one Identity
 */
class Identity {
  /**
   * Constructor
   * @param {string} handler User handler
   * @param {address} address User DID address
   * @param {Parity} parity Parity connexion
  */
  constructor (handler, address, level, parity) {
    this.parity = parity
    this.handler = handler
    this.address = address
    this.level = level

    // Get contract.
    this.contract = this.parity.getContract(address, IdentityContract.abi)
  }

  /**
   * set a Wallet for the contract
   * @param {Wallet} wallet
   */
  setWallet (wallet) {
    this.contract = this.contract.connect(wallet)
  }

    /**
   * Set Address from key/store in the Identity contract
   * @param {*} key
   * @param {*} value
   */
  async setAddress (key, value) {
    const data = await this.contract.setAddress( ethers.utils.formatBytes32String(key), value)
    return data
  }

  /**
   * Get Address from key/store in the Identity contract
   * @param {*} key
   */
  async getAddress (key) {
    const data = await this.contract.getAddress(ethers.utils.formatBytes32String(key))
    return data
  }

  /**
   * Get Data from key/store in the Identity contract
   * @param {*} key
   * @param {*} value
   */
  async setData (key, value) {
    const data = await this.contract.setData(
      ethers.utils.formatBytes32String(key),
      ethers.utils.formatBytes32String(value)
    )
    return data
  }

  /**
   * Get Data from key/store in the Identity contract
   * @param {*} key
   */
  async getData (key) {
    const data = await this.contract.getData(ethers.utils.formatBytes32String(key))
    return data
  }

  /**
   * Get Data from key/store in the Identity contract
   * @param {*} key
   */
  async getStringData (key) {
    const data = await this.contract.getData(ethers.utils.formatBytes32String(key))
    return ethers.utils.parseBytes32String(data)
  }
}

module.exports = Identity
