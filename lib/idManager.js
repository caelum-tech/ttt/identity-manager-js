let Identity = require('./identity')
const IdManagerContract = require('@caelum-tech/identity-manager-contracts/build/contracts/IdManager.json')

/**
 * Class Factory
 * Factory for all identities
 */
class IdManager {
  /**
   * Constructor
   * @param {Parity} parity Parity connexion
   * @param {ethers.wallet} wallet Wallet to interact with the contract
  */
  constructor (parity, wallet = false) {
    this.parity = parity
    this.wallet = wallet
    this.contract = false
  }

  /**
   * Deploy IdFactory contract.
   */
  async deployFactory () {
    this.contract = await this.parity.deployContract(this.wallet, IdManagerContract, [])
  }

  /**
   * Adds a new Identity inside the factory.
   * @param {string} handler Handler for the new user
   * @param {address} owner Address owner for this new identity
   */
  async addIdentity (handler, owner) {
    if (!this.contract || !this.wallet) return false
    await this.contract.createIdentity(handler, owner)
      .catch((_err) => {
        return false
      })
    let identity = await this.resolve(handler)
    return new Identity(handler, identity, 1, this.parity)
  }

  /**
   * Retrieves one identity by iths handler.
   * @param {string} handler Handler for the user
   * @param {Wallet} wallet Wallet to be connected to the contract (if any)
   */
  async getIdentity (handler) {
    if (!this.contract) return false
    const idAddr = await this.contract.resolve(handler)
    if (idAddr[0] !== false) {
      try {
        return new Identity(handler, idAddr[0], idAddr[1], this.parity)
      } catch (_e) {
        return false
      }
    }
    return false
  }

  /**
   * Adds a new Identity inside the factory.
   * @param {string} handler Handler for the new user.
   * @return {address} DID address for that handler.
   */
  async resolve (handler) {
    if (!this.contract) return false
    const addr = await this.contract.resolve(handler)
    return (addr[0])
  }

  /**
   * Adds a new Identity inside the factory.
   * @param {string} handler Handler for the new user.
   * @return {address} DID address for that handler.
   */
  async population (handler) {
    if (!this.contract) return false
    const numIdentifiers = await this.contract.numIdentifiers()
    return (numIdentifiers)
  }
}

module.exports = IdManager
